# Terragrunt with the Terraform Template

GitLab provides [Terraform CI Templates](https://docs.gitlab.com/ee/user/infrastructure/iac/#use-a-terraform-template)
which automatically setup up the Terraform backend to connect to the 
[GitLab-managed Terraform State](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html).

This repository provides an "unofficial" example of to leverage the aforementioned Terraform template,
but use it with [Terragrunt](https://terragrunt.gruntwork.io/) instead.

You may even be able to *just use* the `.gitlab-ci.yml` as a CI template for your own project, by including it:

```yaml
include: 
  remote: 'https://gitlab.com/timofurrer/terraform-template-terragrunt/raw/main/.gitlab-ci.yml'
```

## Concept

The fundamental idea of this template is to source the `gitlab-terraform` script
and run `terragrunt` commands directly. 

## Backend Setup

One major advantage of the [Terraform CI Templates](https://docs.gitlab.com/ee/user/infrastructure/iac/#use-a-terraform-template)
is that is sets up an HTTP Backend so that no manual steps other than declaring an "empty" backend is required.

Because every Terragrunt module needs it's own state this requires a little bit of work. Fortunately, Terragrunts promises
us a DRY config so we can easily generate `backend.tf` files for all modules. 
Use the following `terragrunt.hcl` in your stack root directory to generate the `backend.tf` files:

```hcl
generate "backend" {
  path      = "backend.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
terraform {
  backend "http" {
    address        = "${get_env("TF_STATE_BASE_ADDRESS")}/${path_relative_to_include()}"
    lock_address   = "${get_env("TF_STATE_BASE_ADDRESS")}/${path_relative_to_include()}/lock"
    unlock_address = "${get_env("TF_STATE_BASE_ADDRESS")}/${path_relative_to_include()}/lock"
  }
}
EOF
}
```

The magic here is that `${path_relative_to_include()}` resolves to the directory name (with no nesting) so that
each Terraform state of a module is named according to its directory name. You can ease the entire scripting capabilities
of HCL and Terragrunt to generate state names that work for your case.

In addition, every module needs to include this root module:

```hcl
include "root" {
  path = find_in_parent_folders()
}
```

## Locking

Different than the [Terraform CI Templates](https://docs.gitlab.com/ee/user/infrastructure/iac/#use-a-terraform-template)
the pipeline provided here requires to manually generate consistent lockfiles and track them in Version Control - 
which is the recommendation from HashiCorp. Read more about this [here](https://developer.hashicorp.com/terraform/language/files/dependency-lock).

You can use the `make lock` command to do that locally (currently Linux and macOS locks are generated).